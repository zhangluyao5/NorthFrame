#include "n_frame.h"

#include "windows.h"
#include "stdio.h"

#include "fsm_qande.h"

#define KEY_VALUE(_key) ((GetKeyState(_key) >= 0) ? NF_Bool_False : NF_Bool_True )

/* 信号产生者 */
void Test_Key_Process(void)
{
	static NF_Bool last_q_val = NF_Bool_False;
	static NF_Bool last_e_val = NF_Bool_False;

	NF_Bool then_q_val;
	NF_Bool then_e_val;

	then_q_val = KEY_VALUE('Q');
	then_e_val = KEY_VALUE('E');

	/* Q键事件处理 */
	if ((last_q_val == NF_Bool_False) && (KEY_VALUE('Q') == NF_Bool_True))
	{
		NF_FSM_Dispatch(&FSM_QandE, NF_FSM_Event("Q_DOWN"));
	} 
	else if ((last_q_val == NF_Bool_True) && (KEY_VALUE('Q') == NF_Bool_False))
	{
		NF_FSM_Dispatch(&FSM_QandE, NF_FSM_Event("Q_UP"));
	}

	/* E键事件处理 */
	if ((last_e_val == NF_Bool_False) && (KEY_VALUE('E') == NF_Bool_True))
	{
		NF_FSM_Dispatch(&FSM_QandE, NF_FSM_Event("E_DOWN"));
	} 
	else if ((last_e_val == NF_Bool_True) && (KEY_VALUE('E') == NF_Bool_False))
	{
		NF_FSM_Dispatch(&FSM_QandE, NF_FSM_Event("E_UP"));
	}

	last_q_val = then_q_val;
	last_e_val = then_e_val;
}

void IDLE_TO_Q(void)
{
	printf("state translate : IDLE -> Q_DOWN\n");
}

void Q_TO_QE(void)
{
	printf("state translate : Q_DOWN -> QE_DOWN\n");
}

void QE_TO_IDLE(void)
{
	printf("state translate : QE_DOWN -> IDLE\n");
}

void QE_TO_Q(void)
{
	printf("state translate : QE_DOWN -> Q_DOWN\n");
}

void Q_TO_IDLE(void)
{
	printf("state translate : Q_DOWN -> IDLE\n");
}

int main(void)
{
	for (;;)
	{
		Test_Key_Process();
	}
}
